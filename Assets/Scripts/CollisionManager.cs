using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Goes on the wire object and the ring object must be precised. A life will be removed on collision
/// </summary>
public class CollisionManager : MonoBehaviour
{
    public GameObject ring;
    // Gets called at the start of the collision 
    void OnCollisionEnter(Collision collision)
    {
        Debug.Log("Entered collision with " + collision.gameObject.name);
        // if (collision.gameObject == ring){  
        //     Debug.Log("Entered collision with " + collision.gameObject.name);
        //     LifeManager.current.removeLife();
        // }
        collision.gameObject.GetComponent<Renderer>().material.color = Color.green;
    }

    // Gets called during the collision
    void OnCollisionStay(Collision collision)
    {
        // Debug.Log("Colliding with " + collision.gameObject.name);
        // if (collision.gameObject == ring){
        
        // }
    }

    // Gets called when the object exits the collision
    void OnCollisionExit(Collision collision)
    {
        
        if (collision.gameObject == ring){  
            Debug.Log("Exited collision with " + collision.gameObject.name);
            LifeManager.current.removeLife();
            collision.gameObject.GetComponent<Renderer>().material.color = Color.white;
        }
    }
}
