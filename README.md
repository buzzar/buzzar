Ce projet s'inscrit dans le cadre de l'UE E - Réalité Augmentée 2024. 

Visé à être réutilisé pour des recherches sur les comportements et réactions d'utilisateurs en réalité augmentée, le projet est sous la forme d'un jeu (conception sur Unity) où le joueur doit doit suivre la trajectoire d'un fil de fer avec un anneau au bout d'un bâton et ce, sans le toucher.

L'organisation du projet a été confié aux étudiants qui devaient : 
- S'organiser
- Définir un cahier des charges
- Se partager les rôles et les tâches
- Concevoir et implémenter une solution

Ce qui est ressorti de ce projet est donc une application qui : 
- Détecte des marqueurs au bout d'un fil pour faire apparaître un anneau en réalité augmentée
- Affiche différentes formes de fils en fonction de la difficulté du niveau choisi
- Réagit aux mouvements de l'utilisateur (avec son bâton) 
- Réagit quand l'anneau touche le fil en enlevant une des trois vies
- Permet d'avoir une situation d'échec (GameOver au bout de trois vies perdues) et réussite
- Est user-friendly avec une interface simple et des composants familiers à l'utilisateur.

Encadrants : 
- Etienne PEILLARD
- Julien CAUQUIS

Elèves (TAF IHM):
- Lucas BUTERY
- Noé LOISIL
- Mohamed Taher MAALEJ
- Sacha NERSON
- Selma SABRI
- Matias TRAN-BINH
