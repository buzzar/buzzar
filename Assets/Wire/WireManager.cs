using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Splines;


/// <summary>
/// Permet de charger et decharget les fils en fonction du niveau voulu
/// Les niveaux sont les enfants de cette objet
/// </summary>


public class WireManager : MonoBehaviour
{
    [System.NonSerialized]
    int currentLevel;

    int CurrentLevel // niveau qui est affich� actuellement, 0 si aucun niveau n'est affich�
    {
        get
        {
            return currentLevel + 1;
        }
        set
        {
            StartLevel(value);
        }
    }
    float loadingLevel; // taux de chargement du niveau actuel, vaut 1 quand le niveau est charg�, � but graphique uniquement

    [SerializeField]
    float TimeLoading = 5; // temps de chargement d'un niveau en secondes

    private void Awake()
    {
        currentLevel = 0;
        loadingLevel = 1;
    }
    void Start()
    {
        StartLevel(1); // Afin de tester, lance le niveau 1
    }

    /* Permet de lancer un niveau en dechrgant l'ancien */
    public void StartLevel(int level)
    {
        if(currentLevel>=0)
            transform.GetChild(currentLevel).gameObject.SetActive(false);
        currentLevel = level-1;
        loadingLevel = 0;
        if (currentLevel >= 0)
        {
            transform.GetChild(currentLevel).gameObject.SetActive(true); // active le fil concern�
            transform.GetChild(currentLevel).GetComponent<SplineExtrude>().Range = new Vector2(0, 0); // commence le chargement du niveau � 0
            transform.GetChild(currentLevel).GetComponent<SplineExtrude>().Rebuild(); // actualsie le mesh du fil
        }


    }

    // Update is called once per frame
    void Update()
    {
        if(loadingLevel < 1) // si le niveau n'est pass charg�
        {
            loadingLevel = Mathf.Min(1, loadingLevel + Time.deltaTime / TimeLoading); // on charge legerement le niveau
            transform.GetChild(currentLevel).GetComponent<SplineExtrude>().Range = new Vector2(0, loadingLevel);
            transform.GetChild(currentLevel).GetComponent<SplineExtrude>().Rebuild(); // actualsie le mesh du fil
        }
    }
}
