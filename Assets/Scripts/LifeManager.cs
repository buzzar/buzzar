using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.SceneManagement;
using TMPro;



/// <summary>
/// Manage the number of life and the display of the heart and the empty hearts.
/// Must be placed once on any object (place on lifeManager currently)
/// Also play the touch sound. Stoping the bakckground sound and playing the game over sound when loosing is yet to be implemented
/// </summary>
public class LifeManager : MonoBehaviour
{
    public GameObject[] hearts;

    public TextMeshProUGUI gameOverText;
    public static LifeManager current;

    public AudioSource gameoverSound;
    public AudioSource touchSound;

    public AudioSource backgroundSound;

    private Sprite emptyHeartSprite;

    private DateTime derniereAction;
    private void Awake()
    {
        if (current == null)
        {
            current = this;
        }
        else
        {
            Destroy(obj: this);
        }
    }
    private int nbLife = 3;

    // Déclaration de l'événement
    public event EventHandler NbLifeChangee;

    public int NbLife
    {
        get { return nbLife; }
        set
        {
            if (value != nbLife)
            {
                nbLife = value;

                // Appeler l'événement lorsque la valeur change
                OnValeurChange();
            }
        }
    }

    // Méthode pour déclencher l'événement
    protected virtual void OnValeurChange()
    {
        // Vérifier si des abonnés existent
        NbLifeChangee?.Invoke(this, EventArgs.Empty);
        Debug.Log(nbLife+" lives left");
    }

    public void removeLife(){
        if (EstPasseeDeuxSecondes())
        {
            // Effectuez votre action ici
            NbLife = NbLife - 1;
            // Mettez à jour la variable de la dernière action
            derniereAction = DateTime.Now;
        }

    }

    bool EstPasseeDeuxSecondes()
    {
        // Calculez la différence de temps entre maintenant et la dernière action
        TimeSpan difference = DateTime.Now - derniereAction;

        // Vérifiez si au moins 2 secondes se sont écoulées
        return difference.TotalSeconds >= 2;
    }

    public void actualiseVisibleLives(object sender, EventArgs e){
        touchSound.Play();
        for(int i = 0;i != hearts.Length;++i)
        {
            hearts[i].transform.GetChild(0).gameObject.SetActive(nbLife >= 3-i);
            hearts[i].transform.GetChild(1).gameObject.SetActive(nbLife < 3-i);
        }
        showParticles(nbLife);

        if(nbLife<=0){
            gameOverText.enabled = true;
            backgroundSound.Stop();
            gameoverSound.Play();
            StartCoroutine(ReloadSceneAfterDelay(5f));
            
            
        }
    }

    IEnumerator ReloadSceneAfterDelay(float delai)
    {
        // Attendre pendant le délai spécifié
        yield return new WaitForSeconds(delai);
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }


    public void showParticles(int n){
            ParticleSystem particule = hearts[2-n].GetComponentInChildren<ParticleSystem>();
            Debug.Log("particule found");
            Debug.Log(particule);
            particule.Play();
    }
    void Start()
    {
        derniereAction = DateTime.Now;
        NbLifeChangee+=actualiseVisibleLives;
    }


    // Update is called once per frame
    void Update()
    {
        
    }
}
